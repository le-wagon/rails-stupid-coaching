Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

get 'ask', to: 'questions#ask', as: :question_ask
get 'answer', to: 'questions#answer', as: :question_answer

post 'answer', to: 'questions#answer', as: :question_meth

root to: 'questions#home'
end
